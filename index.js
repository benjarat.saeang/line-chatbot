const AWS = require("aws-sdk");
const axios = require("axios");
const flexData = require("./flex-message.json");

AWS.config.update({ region: "ap-southeast-1" });

exports.handler = async (event) => {
  console.log(event.body);
  const events = JSON.parse(event.body);
  console.log("EVENTS>>>> ", events);

  const replyToken = events.events[0].replyToken;
  const userId = events.events[0].source.userId;
  console.log("userId: ", userId);

  await putMessagesData(events);

  const userProfile = await getUserProfile(userId);
  console.log("USER PROFILE>>>> ", userProfile);

  const replyData = JSON.stringify({
    replyToken: replyToken,
    messages: [
      {
        type: "text",
        text: `Hello ${userProfile.displayName}`,
      },
      flexData,
    ],
  });
  await replyMessage(replyData);
  return null;
};

const putMessagesData = (events) => {
  const documentClient = new AWS.DynamoDB.DocumentClient();
  const message = events.events[0].message;
  var recievedMessage = "";

  switch (message.type) {
    case "sticker":
      recievedMessage = "(sticker)";
      break;
    case "image":
      recievedMessage = "(image)";
      break;
    case "video":
      recievedMessage = "(video)";
      break;
    case "audio":
      recievedMessage = "(audio)";
      break;
    case "location":
      recievedMessage = "(location)";
      break;
    default:
      recievedMessage = message.text;
  }
  console.log(
    "PUT DATA>>> ",
    events.events[0].source.userId,
    events.events[0].timestamp,
    recievedMessage
  );

  const params = {
    TableName: "Messages",
    Item: {
      uid: events.events[0].source.userId,
      createdAt: events.events[0].timestamp,
      message: recievedMessage,
    },
  };

  try {
    const data = documentClient.put(params).promise();
    console.log("SAVE TO DynamoDB!");
  } catch (err) {
    console.log(err);
  }
};

const getUserProfile = async (userId) => {
  const config = {
    method: "get",
    url: `https://api.line.me/v2/bot/profile/${userId}`,
    headers: {
      Authorization:
        "Bearer {m16Yf8ejgbsFiaNM/ZIm8af0CbAJjJhfP4dpDIUdzkgRrPmcJcmpjXkQd8XIzMWgkXmYqJ+1GIJAX12kUusf7hpw9EDO/G/59pcCBPT5dYYS6O60ZsVD5hF3rWk1v4G4artoG4TkKfdUOI7quCVVGQdB04t89/1O/w1cDnyilFU=}",
    },
  };

  try {
    const response = await axios(config);
    console.log("response: ", response);
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

const replyMessage = async (replyData) => {
  const config = {
    method: "post",
    url: `https://api.line.me/v2/bot/message/reply`,
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(replyData),
      Authorization:
        "Bearer {m16Yf8ejgbsFiaNM/ZIm8af0CbAJjJhfP4dpDIUdzkgRrPmcJcmpjXkQd8XIzMWgkXmYqJ+1GIJAX12kUusf7hpw9EDO/G/59pcCBPT5dYYS6O60ZsVD5hF3rWk1v4G4artoG4TkKfdUOI7quCVVGQdB04t89/1O/w1cDnyilFU=}",
    },
    data: replyData,
  };

  try {
    const response = await axios(config);
    console.log("response: ", response);
  } catch (error) {
    console.error(error);
  }
};
